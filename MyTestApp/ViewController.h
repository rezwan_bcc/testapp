//
//  ViewController.h
//  MyTestApp
//
//  Created by Admin on 12/27/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *popularCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@end

