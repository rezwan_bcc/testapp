//
//  PopularItem.h
//  MyTestApp
//
//  Created by Admin on 12/27/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopularItem : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *nameImage;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSNumber *count;

@end
