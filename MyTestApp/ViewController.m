//
//  ViewController.m
//  MyTestApp
//
//  Created by Admin on 12/27/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ViewController.h"
#import "PopularCollectionViewCell.h"
#import "PopularItem.h"
#import "DataTableViewCell.h"

#define REUSE_IDENTIFIER_COLLECTION @"CollectionViewCell"
#define REUSE_IDENTIFIER_TABLE @"DataCell"

@interface ViewController () {
    NSMutableArray<PopularItem*> *_popularList;
    NSMutableArray<PopularItem*> *_dataList;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _popularCollectionView.delegate = self;
    _popularCollectionView.dataSource = self;
    _dataTableView.delegate = self;
    _dataTableView.dataSource = self;
    // Do any additional setup after loading the view, typically from a nib.
    [_popularCollectionView registerNib:[UINib nibWithNibName:@"PopularCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:REUSE_IDENTIFIER_COLLECTION];
    [_dataTableView registerNib:[UINib nibWithNibName:@"DataTableViewCell" bundle:nil] forCellReuseIdentifier:REUSE_IDENTIFIER_TABLE];
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _popularList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PopularCollectionViewCell *cell = (PopularCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:REUSE_IDENTIFIER_COLLECTION forIndexPath:indexPath];
    PopularItem *popular = [_popularList objectAtIndex:indexPath.item];
    cell.nameLabel.text = popular.name;
    cell.countLabel.text = popular.count.stringValue;
    cell.typeLabel.text = popular.type;
    NSURL *url = [NSURL URLWithString:popular.image];
    
    NSURLSessionTask *imagetask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.image.image = image;
                });
            }
        }
    }];
    [imagetask resume];
    return cell;
}

#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DataTableViewCell *cell = (DataTableViewCell*)[tableView dequeueReusableCellWithIdentifier:REUSE_IDENTIFIER_TABLE forIndexPath:indexPath];
    PopularItem *popular = [_popularList objectAtIndex:indexPath.item];
    cell.nameLabel.text = popular.name;
    NSURL *url = [NSURL URLWithString:popular.image];
    
    NSURLSessionTask *imagetask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.image.image = image;
                });
            }
        }
    }];
    [imagetask resume];
    
    url = [NSURL URLWithString:popular.nameImage];
    imagetask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.nameImageView.image = image;
                });
            }
        }
    }];
    [imagetask resume];
    
    return cell;
}

#pragma mark - TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 231;
}


#pragma mark - DataFetch
- (void)fetchData {
    NSURL *url = [[NSURL alloc] initWithString:@"http://logic-poll-api-test.ggdev.xyz/dummy.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *dataTask= [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        NSLog(@"%@",dic.description);
        NSArray *popularArray = dic[@"popular"];
        _popularList = [[NSMutableArray alloc] initWithCapacity:popularArray.count];
        
        for (NSDictionary *d in popularArray) {
            PopularItem *popular = [[PopularItem alloc] init];
            popular.name = d[@"name"];
            popular.image = d[@"image"];
            popular.nameImage = d[@"name_image"];;
            popular.type = d[@"type"];
            popular.count = d[@"count"];
            [_popularList addObject:popular];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_popularCollectionView reloadData];
        });
        
        popularArray = dic[@"data"];
        _dataList = [NSMutableArray arrayWithCapacity:popularArray.count];
        for (NSDictionary *d in popularArray) {
            PopularItem *popular = [[PopularItem alloc] init];
            popular.name = d[@"name"];
            popular.image = d[@"image"];
            popular.nameImage = d[@"name_image"];;
            popular.type = d[@"type"];
            popular.count = d[@"count"];
            [_dataList addObject:popular];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_dataTableView reloadData];
        });
    }];
    [dataTask resume];
}

@end









